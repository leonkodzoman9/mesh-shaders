#pragma once

#include "Includes.h"

#include "Vector.h"



struct BoundingBox {

	Vector3f minimum;
	Vector3f maximum;
};

struct BoundingSphere {

	Vector3f position;
	float radius = 0;
};



