#include "BufferDescriptorGL.h"

#include "BufferGL.h"

#include "BufferDescriptorLayout.h"



BufferDescriptorGL::BufferDescriptorGL() {

    glCreateVertexArrays(1, &this->ID);
}
BufferDescriptorGL::~BufferDescriptorGL() {

    glDeleteVertexArrays(1, &this->ID);
}

BufferDescriptorGL::BufferDescriptorGL(BufferDescriptorGL&& rhs) noexcept {

    if (this != &rhs) {

        this->ID = std::exchange(rhs.ID, 0);
    }
}
BufferDescriptorGL& BufferDescriptorGL::operator = (BufferDescriptorGL&& rhs) noexcept {

    if (this != &rhs) {

        glDeleteVertexArrays(1, &this->ID);

        this->ID = std::exchange(rhs.ID, 0);
    }

    return *this;
}

void BufferDescriptorGL::setLayout(std::initializer_list<BDLayoutGL> segments, BufferGL& indexBuffer) {
    
    for (int i = 0; i < segments.size(); i++) {   

        const BDLayoutGL& segment = *(segments.begin() + i);

        int layoutSize = BufferDescriptorLayout::getLayoutSize(segment.layout);

        if (layoutSize <= 4) {
            glEnableVertexArrayAttrib(this->ID, segment.bindingPoint);
            glVertexArrayVertexBuffer(this->ID, segment.bindingPoint, segment.dataSource->ID, 0, segment.bufferStride);
            glVertexArrayAttribFormat(this->ID, segment.bindingPoint, layoutSize, GL_FLOAT, GL_FALSE, segment.bufferOffset);
            glVertexArrayAttribBinding(this->ID, segment.bindingPoint, segment.bindingPoint);
        }

        if (layoutSize == 16) {
            for (int i = 0; i < 4; i++) {
                glEnableVertexArrayAttrib(this->ID, segment.bindingPoint + i);
                glVertexArrayVertexBuffer(this->ID, segment.bindingPoint + i, segment.dataSource->ID, 16ull * i, segment.bufferStride);
                glVertexArrayAttribFormat(this->ID, segment.bindingPoint + i, 4, GL_FLOAT, GL_FALSE, segment.bufferOffset);
                glVertexArrayAttribBinding(this->ID, segment.bindingPoint + i, segment.bindingPoint + i);
                glVertexArrayBindingDivisor(this->ID, segment.bindingPoint + i, 1);
            }
        }
    }

    glVertexArrayElementBuffer(this->ID, indexBuffer.ID);
}


