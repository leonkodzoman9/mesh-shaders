#pragma once

#include "Includes.h"

#include "BufferDescriptorLayout.h"



class BufferGL;



struct BDLayoutGL {

	BufferDescriptorLayout::Layout layout;

	BufferGL* dataSource = nullptr;

	uint32_t bindingPoint = 0;
	uint32_t bufferOffset = 0;
	uint32_t bufferStride = 0;
};



class BufferDescriptorGL {

public:

	uint32_t ID;

public:

	BufferDescriptorGL();
	~BufferDescriptorGL();

	BufferDescriptorGL(const BufferDescriptorGL& rhs) = delete;
	BufferDescriptorGL& operator=(const BufferDescriptorGL& rhs) = delete;
	BufferDescriptorGL(BufferDescriptorGL&& rhs) noexcept;
	BufferDescriptorGL& operator=(BufferDescriptorGL&& rhs) noexcept;

	void setLayout(std::initializer_list<BDLayoutGL> segments, BufferGL& indexBuffer);
};
