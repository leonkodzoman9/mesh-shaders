#include "BufferDescriptorLayout.h"



uint32_t BufferDescriptorLayout::getLayoutSize(Layout type) {

	switch (type) {
	case Layout::FLOAT:		return 1;
	case Layout::FLOAT2:	return 2;
	case Layout::FLOAT3:	return 3;
	case Layout::FLOAT4:	return 4;
	case Layout::MATRIX4:	return 16;
	}

	return 0;
}