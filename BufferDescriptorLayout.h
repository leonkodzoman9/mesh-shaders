#pragma once

#include "Includes.h"



namespace BufferDescriptorLayout {

	enum class Layout {

		FLOAT,
		FLOAT2,
		FLOAT3,
		FLOAT4,

		MATRIX4
	};

	uint32_t getLayoutSize(Layout type);
}



