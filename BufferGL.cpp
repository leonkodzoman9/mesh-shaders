#include "BufferGL.h"



BufferGL::BufferGL() {

    glCreateBuffers(1, &this->ID);
}
BufferGL::~BufferGL() {

    glDeleteBuffers(1, &this->ID);
}

BufferGL::BufferGL(BufferGL&& rhs) noexcept {

    if (this != &rhs) {

        this->ID = std::exchange(rhs.ID, 0);
    }
}
BufferGL& BufferGL::operator=(BufferGL&& rhs) noexcept {

    if (this != &rhs) {

        glDeleteBuffers(1, &this->ID);

        this->ID = std::exchange(rhs.ID, 0);
    }

    return *this;
}

void BufferGL::setData(void* data, ByteCount bytes, uint32_t usage) {

    glNamedBufferData(this->ID, bytes, data, usage);
}
void BufferGL::updateData(void* data, ByteCount bytes, ByteCount offset) {

    glNamedBufferSubData(this->ID, offset, bytes, data);
}
void BufferGL::copyDataFrom(BufferGL& source, ByteCount readOffset, ByteCount writeOffset, ByteCount readSize) {

    glCopyNamedBufferSubData(source.ID, this->ID, readOffset, writeOffset, readSize);
}

