#pragma once

#include "Includes.h"



class BufferGL {

public:

    uint32_t ID;

public:

    BufferGL();
    ~BufferGL();

    void setData(void* data, ByteCount size, uint32_t usage);
    void updateData(void* data, ByteCount size, ByteCount offset);
    void copyDataFrom(BufferGL& source, ByteCount readOffset, ByteCount writeOffset, ByteCount readSize);

    BufferGL(const BufferGL& rhs) = delete;
    BufferGL& operator=(const BufferGL& rhs) = delete;
    BufferGL(BufferGL&& rhs) noexcept;
    BufferGL& operator=(BufferGL&& rhs) noexcept;
};










