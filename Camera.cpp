#include "Camera.h"

#include "Frustum.h"



Camera::Camera() {

    this->yaw = 0.0f;
    this->pitch = 0.0f;

    this->fov = 0;

    this->near = 0;
    this->far = 0;
    this->cascadeCount = 0;
}
Camera::Camera(Vector3f position, float yaw, float pitch, float fov, float near, float far, int subfrustumCount) {

    this->position = position;
    this->yaw = front.yaw() * (180.0f / std::numbers::pi_v<float>);
    this->pitch = front.pitch() * (180.0f / std::numbers::pi_v<float>);

    this->fov = fov;

    this->near = near;
    this->far = far;
    this->cascadeCount = subfrustumCount;

    this->updateVectors();
}
Camera::Camera(Vector3f position, Vector3f front, float fov, float near, float far, int subfrustumCount) {

    this->position = position;
    this->yaw = front.yaw() * (180.0f / std::numbers::pi_v<float>);
    this->pitch = front.pitch() * (180.0f / std::numbers::pi_v<float>);

    this->fov = fov;

    this->near = near;
    this->far = far;
    this->cascadeCount = subfrustumCount;

    this->updateVectors();
}

void Camera::move(Vector3f direction, float frametime) {

    this->position += direction * frametime;
}
void Camera::rotate(Vector2f delta, float timestep, bool constrainPitch) {

    this->yaw += delta.x * timestep;
    this->pitch -= delta.y * timestep;

    if (constrainPitch) {
        if (this->pitch > 89.0f) {
            this->pitch = 89.0f;
        }
        if (this->pitch < -89.0f) {
            this->pitch = -89.0f;
        }
    }

    this->updateVectors();
}

Matrix4f Camera::getViewMatrix() const {

    return GML::View(this->position, this->front, this->right, this->up);
}
Matrix4f Camera::getCascadeProjectionMatrix(float aspectRatio, int subfrustumIndex) const {

    float near = subfrustumIndex == 0 ? this->near : this->cascadeCutoffDistances.data[subfrustumIndex - 1];
    float far = this->cascadeCutoffDistances.data[subfrustumIndex];

    return GML::Projection(aspectRatio, this->fov,  near, far);
}
Matrix4f Camera::getProjectionMatrix(float aspectRatio) const {

    return GML::Projection(aspectRatio, this->fov, this->near, this->far);
}

Frustum Camera::getFrustumPlanes(float aspectRatio) const {

    Frustum frustum;

    float height = this->far * std::tan(this->fov / (180.0f / std::numbers::pi_v<float>) / 2);
    float width = height * aspectRatio;

    frustum.planes[0] = { this->position + this->front * this->near , -this->front };
    frustum.planes[1] = { this->position, GML::Cross(this->front * this->far + this->right * width, this->up) };
    frustum.planes[2] = { this->position + this->front * this->far, this->front };
    frustum.planes[3] = { this->position, GML::Cross(this->front * this->far - this->right * width, -this->up) };
    frustum.planes[4] = { this->position, GML::Cross(this->front * this->far + this->up * height, -this->right) };
    frustum.planes[5] = { this->position, GML::Cross(this->front * this->far - this->up * height, this->right) };

    std::for_each(frustum.planes.begin(), frustum.planes.end(), [](Plane& plane) { plane.normal = GML::Normalize(plane.normal); });

    return frustum;
}



void Camera::updateVectors() {

    float sinPitch = std::sin(this->pitch / (180.0f / std::numbers::pi_v<float>));
    float cosPitch = std::cos(this->pitch / (180.0f / std::numbers::pi_v<float>));

    float sinYaw = std::sin(this->yaw / (180.0f / std::numbers::pi_v<float>));
    float cosYaw = std::cos(this->yaw / (180.0f / std::numbers::pi_v<float>));

    this->front = Vector3f(cosPitch * cosYaw, sinPitch, cosPitch * sinYaw);
    this->right = GML::Normalize(GML::Cross(this->front, Vector3f::Up()));
    this->up = GML::Normalize(GML::Cross(this->right, this->front));
}
