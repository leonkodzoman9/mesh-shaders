#pragma once

#include "Includes.h"

#include "Vector.h"
#include "Matrix.h"



struct Frustum;



class Camera {

public:

    Vector3f position;
    Vector3f front;
    Vector3f up;
    Vector3f right;

    float yaw;
    float pitch;

    float fov;

    float near;
    float far;

    int cascadeCount;
    Vector4f cascadeCutoffDistances;



    Camera();
    Camera(Vector3f position, Vector3f front, float fov = 90, float near = 1, float far = 100, int subfrustumCount = 1);
    Camera(Vector3f position, float yaw, float pitch, float fov = 90, float near = 1, float far = 100, int subfrustumCount = 1);

    void move(Vector3f direction, float frametime);
    void rotate(Vector2f delta, float frametime, bool constrainPitch = true);

    Matrix4f getViewMatrix() const;
    Matrix4f getCascadeProjectionMatrix(float aspectRatio, int subfrustumIndex) const;
    Matrix4f getProjectionMatrix(float aspectRatio) const;

    Frustum getFrustumPlanes(float aspectRatio) const;

private:

    void updateVectors();
};

