#pragma once

#include "Includes.h"

#include "Vector.h"



struct Plane {

	Vector3f position;
	Vector3f normal;
};

struct Frustum {

	std::array<Plane, 6> planes;
};

