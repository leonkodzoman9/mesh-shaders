#include "Keyboard.h"

#include "Window.h"



Keyboard& Keyboard::getInstance() {

	// A static instance becomes a singleton, first time initialized, other times just returned
	static Keyboard instance;

	return instance;
}

void Keyboard::update() {

	GLFWwindow* window = Window::getInstance().getWindow();

	this->currentTime = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::steady_clock::now().time_since_epoch()).count() / 1'000'000.0f;

	for (int i = (int)KeyCode::KEY_FIRST; i <= (int)KeyCode::KEY_LAST; i++) {

		this->previousState[i] = this->currentState[i];
		this->currentState[i] = glfwGetKey(window, getKeyValue(KeyCode(i))) == GLFW_PRESS;

		bool currentState = this->currentState[i];
		bool previousState = this->previousState[i];

		if (!previousState && currentState) {
			this->updatePressTransitionCounter(KeyCode(i), currentTime);
		}

		if (previousState && !currentState) {
			this->updateReleaseTransitionCounter(KeyCode(i), currentTime);
		}
	}
}
void Keyboard::reset() {

	std::fill(this->currentState.begin(), this->currentState.end(), 0);
	std::fill(this->previousState.begin(), this->previousState.end(), 0);

	this->currentTime = 0;
	std::fill(this->pressTransitionCounter.begin(), this->pressTransitionCounter.end(), std::pair<float, float>(0, 0));
	std::fill(this->releaseTransitionCounter.begin(), this->releaseTransitionCounter.end(), std::pair<float, float>(0, 0));
}

bool Keyboard::held(KeyCode key) const {

	return this->currentState[(int)key];
}
bool Keyboard::pressed(KeyCode key) const {

	return this->currentState[(int)key] && !this->previousState[(int)key];
}
bool Keyboard::released(KeyCode key) const {

	return !this->currentState[(int)key] && this->previousState[(int)key];
}

float Keyboard::heldFor(KeyCode key) const {

	return this->held(key) ? this->currentTime - this->pressTransitionCounter[(int)key].second : 0;
}
float Keyboard::releasedFor(KeyCode key) const {

	return !this->held(key) ? this->currentTime - this->releaseTransitionCounter[(int)key].second : 0;
}

float Keyboard::pressDelta(KeyCode key) const {

	return this->pressTransitionCounter[(int)key].second - this->pressTransitionCounter[(int)key].first;
}
float Keyboard::releaseDelta(KeyCode key) const {

	return this->releaseTransitionCounter[(int)key].second - this->releaseTransitionCounter[(int)key].first;
}



Keyboard::Keyboard() {

	this->reset();
};

void Keyboard::updatePressTransitionCounter(KeyCode key, float currentTime) {

	this->pressTransitionCounter[(int)key].first = this->pressTransitionCounter[(int)key].second;
	this->pressTransitionCounter[(int)key].second = currentTime;
}
void Keyboard::updateReleaseTransitionCounter(KeyCode key, float currentTime) {

	this->releaseTransitionCounter[(int)key].first = this->releaseTransitionCounter[(int)key].second;
	this->releaseTransitionCounter[(int)key].second = currentTime;
}
