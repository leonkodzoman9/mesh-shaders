#pragma once

#include "Includes.h"

#include "KeyCode.h"



class Keyboard {

private:

	std::array<bool, (int)KeyCode::KEY_LAST + 1> currentState;
	std::array<bool, (int)KeyCode::KEY_LAST + 1> previousState;

	std::array<std::pair<float, float>, (int)KeyCode::KEY_LAST + 1> pressTransitionCounter;
	std::array<std::pair<float, float>, (int)KeyCode::KEY_LAST + 1> releaseTransitionCounter;

	float currentTime;

public:

	static Keyboard& getInstance();

	void update();
	void reset();

	bool held(KeyCode key) const;
	bool pressed(KeyCode key) const;
	bool released(KeyCode key) const;

	float heldFor(KeyCode key) const;
	float releasedFor(KeyCode key) const;

	float pressDelta(KeyCode key) const;
	float releaseDelta(KeyCode key) const;

private:

	Keyboard();
	Keyboard(const Keyboard& rhs) = delete;
	Keyboard& operator=(const Keyboard& rhs) = delete;
	Keyboard(Keyboard&& rhs) noexcept = delete;
	Keyboard& operator=(Keyboard&& rhs) noexcept = delete;

	void updatePressTransitionCounter(KeyCode key, float currentTime);
	void updateReleaseTransitionCounter(KeyCode key, float currentTime);
};


