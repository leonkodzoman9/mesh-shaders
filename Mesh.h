#pragma once

#include "Includes.h"

#include "BoundingVolume.h"
#include "Vector.h"



typedef uint64_t MeshIDType;


// This vertex type is used when dealing with vertex data on the CPU side because the CPU can pad and pack Vector3f values properly
struct VertexCPU {

	Vector3f position;
	Vector3f normal;
	Vector3f uv;
};

// This vertex type is used when dealing with vertex data on the GPU side because the GPU cannot deal with Vector3f values properly
// Use this only when sending data to vertex buffers
struct VertexGPU {

	Vector4f position;
	Vector4f normal;
	Vector4f uv;
};

struct Mesh {

	BoundingSphere boundingSphere;

	std::vector<VertexCPU> vertices;
	std::vector<int> indices;
};



struct MeshletData {

	int vertexIndexCount = 0;
	std::array<int, 64> vertexIndices = { 0 };

	int primitiveCount = 0;
	std::array<uint8_t, 84 * 3> primitiveindices = { 0 };

	BoundingSphere boundingSphere;
	Vector3f normal;
};

struct Meshlet {

	struct {
		uint8_t relativePositionX = 0;
		uint8_t relativePositionY = 0;
		uint8_t relativePositionZ = 0;
		uint8_t relativeRadius = 0;
	};

	struct {
		uint8_t normalX = 0;
		uint8_t normalY = 0;
		uint8_t normalZ = 0;
		uint8_t primitiveCount = 0;
	};

	int vertexIndexCount = 0;
	int vertexIndexOffset = 0;
};

struct MeshletMesh {

	BoundingSphere boundingSphere;

	std::vector<VertexCPU> vertices;
	std::vector<int> vertexIndices;
	std::vector<uint8_t> meshletIndices;
	std::vector<Meshlet> meshlets;
};
