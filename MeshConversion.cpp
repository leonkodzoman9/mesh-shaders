#include "MeshConversion.h"

#include "Vector.h"
#include "Mesh.h"
#include "BoundingVolume.h"
#include "Packing.h"
#include "TriangleVertexGrid.h"
#include "MeshUtility.h"



struct Triangle {
	Vector3i indices;
	std::vector<Triangle*> neighbors;
	bool visited = false;
};



/// <summary>
/// Add a triangle to a meshlet unconditionally
/// </summary>
/// <param name="meshletData :">Meshlet data</param>
/// <param name="indices :">Indices of the new triangle</param>
/// <param name="existingIndices :">Indices of the already present vertices</param>
void addTriangleToMeshlet(MeshletData& meshletData, Vector3i indices, Vector3i existingIndices) {

	Vector3i meshletTriangleIndices;

	// Set indices and appropriate vertices
	int newVertexIndexOffset = 0;
	for (int i = 0; i < 3; i++) {

		int index = existingIndices.data[i];

		if (index == meshletData.vertexIndexCount) {
			meshletTriangleIndices.data[i] = index + newVertexIndexOffset;
			meshletData.vertexIndices[index + newVertexIndexOffset] = indices.data[i];
			newVertexIndexOffset++;
		}
		else {
			meshletTriangleIndices.data[i] = index;
		}
	}

	// Set the new triangle data 
	meshletData.primitiveindices[meshletData.primitiveCount * 3ull + 0ull] = meshletTriangleIndices.x;
	meshletData.primitiveindices[meshletData.primitiveCount * 3ull + 1ull] = meshletTriangleIndices.y;
	meshletData.primitiveindices[meshletData.primitiveCount * 3ull + 2ull] = meshletTriangleIndices.z;
	meshletData.primitiveCount++;
}

/// <summary>
/// Add a triangle to a meshlet
/// Adding the triangle is done only if the triangle can be added
/// </summary>
/// <param name="meshletData :">Meshlet data</param>
/// <param name="indices">Indices of the to-be-added triangle</param>
/// <returns>Returns (true) if the triangle has been added or (false) if it has not been.
/// If adding a triangle failed no meshlet state is changed</returns>
bool addTriangleToMeshlet(MeshletData& meshletData, Vector3i indices) {

	// Check if the indices are already present
	Vector3i existingIndices;
	existingIndices.x = (int)(std::find(meshletData.vertexIndices.begin(), meshletData.vertexIndices.begin() + meshletData.vertexIndexCount, indices.x) - meshletData.vertexIndices.begin());
	existingIndices.y = (int)(std::find(meshletData.vertexIndices.begin(), meshletData.vertexIndices.begin() + meshletData.vertexIndexCount, indices.y) - meshletData.vertexIndices.begin());
	existingIndices.z = (int)(std::find(meshletData.vertexIndices.begin(), meshletData.vertexIndices.begin() + meshletData.vertexIndexCount, indices.z) - meshletData.vertexIndices.begin());

	// Count how many indices are new
	int newVertices = int(existingIndices.x == meshletData.vertexIndexCount) + int(existingIndices.y == meshletData.vertexIndexCount) + int(existingIndices.z == meshletData.vertexIndexCount);

	// If the new triangle cannot fit into the meshlet based on the number of vertices, stop building the meshlet
	if (meshletData.vertexIndexCount + newVertices > meshletData.vertexIndices.size()) { return false; }
	// If the new triangle is not connected to the previous ones, stop building the meshlet
	if (newVertices == 3 && meshletData.vertexIndexCount > 0) { return false; }
	// If the new triangle cannot fit into the meshlet based on the number of primitives, stop building the meshlet
	if (meshletData.primitiveCount * 3ull + 3ull > meshletData.primitiveindices.size()) { return false; }

	// Unconditionally add the triangle to the meshlet
	addTriangleToMeshlet(meshletData, indices, existingIndices);

	meshletData.vertexIndexCount += newVertices;

	return true;
}

/// <summary>
/// Create a meshlet from all relevant data
/// </summary>
/// <param name="meshletData :">Meshlet data</param>
/// <param name="vertexOffset :">Global vertex offset of the meshlet vertex index data</param>
/// <returns>Returns the built meshlet</returns>
Meshlet createMeshlet(MeshletData& meshletData, int vertexOffset) {

	Meshlet meshlet;

	meshlet.relativePositionX = Packing::floatToUNorm8(meshletData.boundingSphere.position.x);
	meshlet.relativePositionY = Packing::floatToUNorm8(meshletData.boundingSphere.position.y);
	meshlet.relativePositionZ = Packing::floatToUNorm8(meshletData.boundingSphere.position.z);
	meshlet.relativeRadius = Packing::floatToUNorm8(meshletData.boundingSphere.radius);

	meshlet.normalX = Packing::floatToSNorm8(meshletData.normal.x);
	meshlet.normalY = Packing::floatToSNorm8(meshletData.normal.y);
	meshlet.normalZ = Packing::floatToSNorm8(meshletData.normal.z);
	meshlet.primitiveCount = meshletData.primitiveCount;

	meshlet.vertexIndexCount = meshletData.vertexIndexCount;
	meshlet.vertexIndexOffset = vertexOffset;

	return meshlet;
}

/// <summary>
/// Add a meshlet to an array of meshlets
/// </summary>
/// <param name="meshletMesh :">MeshletMesh which keeps track of all its meshlets</param>
/// <param name="meshlet :">Meshlet to be added</param>
/// <param name="meshletData :">Meshlet data so that the meshletMesh can be updated</param>
void addMeshletToMeshletMesh(MeshletMesh& meshletMesh, Meshlet& meshlet, MeshletData& meshletData) {

	meshletMesh.vertexIndices.insert(meshletMesh.vertexIndices.end(), meshletData.vertexIndices.begin(), meshletData.vertexIndices.begin() + meshletData.vertexIndexCount);
	meshletMesh.meshletIndices.insert(meshletMesh.meshletIndices.end(), meshletData.primitiveindices.begin(), meshletData.primitiveindices.end());
	meshletMesh.meshlets.push_back(meshlet);
}

/// <summary>
/// Take an array of triangles and produce a graph where each triangle is a node connected to its neighbors
/// </summary>
/// <param name="triangles :">Array of raw triangles</param>
/// <param name="vertices :">Array of vertices to help with grid placement</param>
/// <param name="boundingBox :">Bounding box of all vertices</param>
void connectTriangles(std::vector<Triangle>& triangles, std::vector<VertexCPU>& vertices, BoundingSphere boundingSphere) {

	// Calculate grid essential values and add all triangle points to it

	int cells = (int)std::cbrt(triangles.size()) * 2;

	BoundingBox boundingBox = MeshUtility::convertBoundingSphereToBoundingBox(boundingSphere);
	boundingBox.minimum -= 0.01f;
	boundingBox.maximum += 0.01f;

	TriangleVertexGrid grid(boundingBox, cells);

	for (int i = 0; i < (int)triangles.size(); i++) {
		grid.insertGridPoint({ vertices[triangles[i].indices.x].position, i });
		grid.insertGridPoint({ vertices[triangles[i].indices.y].position, i });
		grid.insertGridPoint({ vertices[triangles[i].indices.z].position, i });
	}

	// For each triangle check its local neighbors and turn them into a graph

	std::for_each(std::execution::seq, triangles.begin(), triangles.end(), [&](Triangle& triangle) {

		int index = (int)(&triangle - &triangles[0]);

		std::vector<TVGPoint> neighbors;
		grid.getNeighboringGridPoints({ vertices[triangles[index].indices.x].position, index }, neighbors);
		grid.getNeighboringGridPoints({ vertices[triangles[index].indices.y].position, index }, neighbors);
		grid.getNeighboringGridPoints({ vertices[triangles[index].indices.z].position, index }, neighbors);

		for (int i = 0; i < (int)neighbors.size(); i++) {

			bool shareAnEdge = MeshUtility::doTrianglesShareAnAdge(triangles[index].indices, triangles[neighbors[i].ID].indices);

			if (!shareAnEdge) {
				continue;
			}

			triangles[index].neighbors.push_back(&triangles[neighbors[i].ID]);
		}
		});
};

/// <summary>
/// Find a triangle which has the smallest number of neighbors AND has not been visited
/// </summary>
/// <param name="triangles :">Array of triangles</param>
/// <returns>Returns a triangle if such is found, otherwise (nullptr)</returns>
Triangle* getBestMatchingTriangle(std::vector<Triangle>& triangles) {

	Triangle* triangle = nullptr;

	int i = 0;
	for (; i < (int)triangles.size(); i++) {
		if (!triangles[i].visited) {
			triangle = &triangles[i];
			break;
		}
	}

	if (triangle == nullptr) {
		return triangle;
	}

	int minCount = (int)triangle->neighbors.size();
	for (; i < (int)triangles.size(); i++) {
		if ((int)triangles[i].neighbors.size() < minCount && !triangles[i].visited) {
			minCount = (int)triangles[i].neighbors.size();
			triangle = &triangles[i];
		}
	}

	return triangle;
}



MeshletMesh MeshConversion::convertMeshToMeshletMeshLinear(Mesh& mesh) {

	MeshletMesh meshletMesh;
	meshletMesh.boundingSphere = mesh.boundingSphere;
	meshletMesh.vertices = mesh.vertices;

	// While there are indices to process

	int currentIndex = 0;
	while (currentIndex < (int)mesh.indices.size()) {

		MeshletData meshletData;

		// Keep adding triangles until the meshlet is full

		while (true) {

			// If there is no next set of indices, stop building the meshlet

			if (currentIndex >= (int)mesh.indices.size()) { break; }

			// Get indices of the next triangle

			Vector3i indices(mesh.indices[currentIndex + 0ull], mesh.indices[currentIndex + 1ull], mesh.indices[currentIndex + 2ull]);

			// If the new triangle was not added stop adding new triangles

			if (!addTriangleToMeshlet(meshletData, indices)) { break; }

			currentIndex += 3;
		}

		// Calculate relevant meshlet data

		meshletData.normal = MeshUtility::calculateMeshletNormal(meshletData, mesh.vertices);
		meshletData.boundingSphere = MeshUtility::calculateBoundingSphere(mesh.vertices, std::span(meshletData.vertexIndices.begin(), meshletData.vertexIndexCount));

		// Scale the meshlet bounding sphere to [0,1] range of the original mesh 

		BoundingBox meshBoundingBox = MeshUtility::convertBoundingSphereToBoundingBox(mesh.boundingSphere);

		Vector3f meshBoundingBoxSize = meshBoundingBox.maximum - meshBoundingBox.minimum;
		meshletData.boundingSphere.position = (meshletData.boundingSphere.position - meshBoundingBox.minimum) / meshBoundingBoxSize;
		meshletData.boundingSphere.radius = meshletData.boundingSphere.radius / meshBoundingBoxSize.magnitude();

		// Create the meshlet and add it
		Meshlet meshlet = createMeshlet(meshletData, meshletMesh.vertexIndices.size());
		addMeshletToMeshletMesh(meshletMesh, meshlet, meshletData);
	}

	return meshletMesh;
}
MeshletMesh MeshConversion::convertMeshToMeshletMeshSmartFast(Mesh& mesh) {

	// Make every triangle a node

	std::vector<Triangle> triangles(mesh.indices.size() / 3);
	for (int i = 0; i < triangles.size(); i++) {
		triangles[i].indices.x = mesh.indices[3ull * i + 0ull];
		triangles[i].indices.y = mesh.indices[3ull * i + 1ull];
		triangles[i].indices.z = mesh.indices[3ull * i + 2ull];
		triangles[i].visited = false;
	}

	// Connect the triangle nodes into a graph

	connectTriangles(triangles, mesh.vertices, mesh.boundingSphere);

	MeshletMesh meshletMesh;
	meshletMesh.boundingSphere = mesh.boundingSphere;
	meshletMesh.vertices = mesh.vertices;

	Triangle* firstTriangle = getBestMatchingTriangle(triangles);
	std::vector<Triangle*> activeNodes = { firstTriangle };

	int meshIndex = 0;
	while (true) {

		// While there are meshlets to be made

		MeshletData meshletData;

		if (activeNodes.size() > 1) {

			// If there are multiple nodes to be evaluated, take the one with the least neighbors

			int index = (int)(std::min_element(activeNodes.begin(), activeNodes.end(), [](Triangle* triangle, Triangle* minimum) {
				return triangle->neighbors.size() < minimum->neighbors.size();
				}) - activeNodes.begin());

				Triangle* active = activeNodes[index];

				activeNodes.clear();
				activeNodes.push_back(active);
		}
		else if (activeNodes.size() == 0) {

			// If there are no nodes to be evaluated, find one in the large triangle array

			Triangle* triangle = getBestMatchingTriangle(triangles);

			// If there is no such triangle stop the creation process

			if (triangle == nullptr) { break; }

			activeNodes.push_back(triangle);
		}

		// While there are unvisited triangles

		while (activeNodes.size() > 0) {

			// Get the triangle with the smallest number of neighbors

			int index = (int)(std::min_element(activeNodes.begin(), activeNodes.end(), [](Triangle* triangle, Triangle* minimum) {
				return triangle->neighbors.size() < minimum->neighbors.size();
				}) - activeNodes.begin());

				Triangle* triangle = activeNodes[index];

				Vector3i indices = triangle->indices;
				if (!addTriangleToMeshlet(meshletData, indices)) {
					break;
				}

				// Update the triangles and add all neighbors to be evaluated

				triangle->visited = true;
				activeNodes.erase(activeNodes.begin() + index);

				for (int i = 0; i < triangle->neighbors.size(); i++) {

					// Remove the link from each neighbor to the current triangle

					auto iterator = std::find(triangle->neighbors[i]->neighbors.begin(), triangle->neighbors[i]->neighbors.end(), triangle);
					triangle->neighbors[i]->neighbors.erase(iterator);

					if (triangle->neighbors[i]->visited) {
						continue;
					}

					if (std::find(activeNodes.begin(), activeNodes.end(), triangle->neighbors[i]) != activeNodes.end()) {
						continue;
					}

					// If the neighbor was not visited and has not been put up for evaluation, add it

					activeNodes.push_back(triangle->neighbors[i]);
				}
		}

		// Calculate relevant meshlet data

		meshletData.normal = MeshUtility::calculateMeshletNormal(meshletData, mesh.vertices);
		meshletData.boundingSphere = MeshUtility::calculateBoundingSphere(mesh.vertices, std::span(meshletData.vertexIndices.begin(), meshletData.vertexIndexCount));

		// Scale the meshlet bounding box to [0,1] range

		BoundingBox meshBoundingBox = MeshUtility::convertBoundingSphereToBoundingBox(mesh.boundingSphere);

		Vector3f meshBoundingBoxSize = meshBoundingBox.maximum - meshBoundingBox.minimum;
		meshletData.boundingSphere.position = (meshletData.boundingSphere.position - meshBoundingBox.minimum) / meshBoundingBoxSize;
		meshletData.boundingSphere.radius = meshletData.boundingSphere.radius / meshBoundingBoxSize.magnitude();

		// Create the meshlet and add it

		Meshlet meshlet = createMeshlet(meshletData, meshletMesh.vertexIndices.size());
		addMeshletToMeshletMesh(meshletMesh, meshlet, meshletData);
	}

	return meshletMesh;
}
MeshletMesh MeshConversion::convertMeshToMeshletMeshSmartSlow(Mesh& mesh) {

	// Make every triangle a node

	std::vector<Triangle> triangles(mesh.indices.size() / 3);
	for (int i = 0; i < triangles.size(); i++) {
		triangles[i].indices.x = mesh.indices[3ull * i + 0ull];
		triangles[i].indices.y = mesh.indices[3ull * i + 1ull];
		triangles[i].indices.z = mesh.indices[3ull * i + 2ull];
		triangles[i].visited = false;
	}

	// Connect the triangles into a graph

	connectTriangles(triangles, mesh.vertices, mesh.boundingSphere);

	MeshletMesh meshletMesh;
	meshletMesh.boundingSphere = mesh.boundingSphere;
	meshletMesh.vertices = mesh.vertices;

	int meshIndex = 0;
	while (true) {

		MeshletData meshletData;

		// Find best triangle to start the meshlet from

		Triangle* triangle = getBestMatchingTriangle(triangles);

		// If there is no such triangle stop the creation process

		if (triangle == nullptr) { break; }

		std::vector<Triangle*> activeNodes = { triangle };

		// While there are unvisited triangles

		while (activeNodes.size() > 0) {

			// Get the triangle with the smallest number of neighbors

			int index = (int)(std::min_element(activeNodes.begin(), activeNodes.end(), [](Triangle* triangle, Triangle* minimum) {
				return triangle->neighbors.size() < minimum->neighbors.size();
				}) - activeNodes.begin());

				Triangle* triangle = activeNodes[index];

				Vector3i indices = triangle->indices;
				if (!addTriangleToMeshlet(meshletData, indices)) {
					break;
				}

				// Update the triangles and add all neighbors to be evaluated

				triangle->visited = true;
				activeNodes.erase(activeNodes.begin() + index);

				for (int i = 0; i < triangle->neighbors.size(); i++) {

					// Remove the link from each neighbor to the current triangle

					auto iterator = std::find(triangle->neighbors[i]->neighbors.begin(), triangle->neighbors[i]->neighbors.end(), triangle);
					triangle->neighbors[i]->neighbors.erase(iterator);

					if (triangle->neighbors[i]->visited) {
						continue;
					}

					if (std::find(activeNodes.begin(), activeNodes.end(), triangle->neighbors[i]) != activeNodes.end()) {
						continue;
					}

					// If the neighbor was not visited and has not been put up for evaluation, add it

					activeNodes.push_back(triangle->neighbors[i]);
				}
		}

		// Calculate relevant meshlet data

		meshletData.normal = MeshUtility::calculateMeshletNormal(meshletData, mesh.vertices);
		meshletData.boundingSphere = MeshUtility::calculateBoundingSphere(mesh.vertices, std::span(meshletData.vertexIndices.begin(), meshletData.vertexIndexCount));

		// Scale the meshlet bounding box to [0,1] range

		BoundingBox meshBoundingBox = MeshUtility::convertBoundingSphereToBoundingBox(mesh.boundingSphere);

		Vector3f meshBoundingBoxSize = meshBoundingBox.maximum - meshBoundingBox.minimum;
		meshletData.boundingSphere.position = (meshletData.boundingSphere.position - meshBoundingBox.minimum) / meshBoundingBoxSize;
		meshletData.boundingSphere.radius = meshletData.boundingSphere.radius / meshBoundingBoxSize.magnitude();

		// Create the meshlet and add it

		Meshlet meshlet = createMeshlet(meshletData, meshletMesh.vertexIndices.size());
		addMeshletToMeshletMesh(meshletMesh, meshlet, meshletData);
	}

	return meshletMesh;
}

