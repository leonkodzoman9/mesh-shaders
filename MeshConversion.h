#pragma once

#include "Includes.h"



struct Mesh;
struct MeshletMesh;



namespace MeshConversion {

	/// <summary>
	/// Convert a normal mesh into a meshlet mesh
	/// Use a simple linear scan of the indices
	/// The linear scan is fast but it does not produce optimal meshlets
	/// On average, each meshlet has <80% triangle occupancy
	/// </summary>
	/// <param name="mesh :">Source mesh</param>
	/// <returns>Returns the converted meshlet mesh</returns>
	MeshletMesh convertMeshToMeshletMeshLinear(Mesh& mesh);

	/// <summary>
	/// Convert a normal mesh into a meshlet mesh
	/// Use a smart scan which is slower then the linear one
	/// but produces almost optimal meshlets.
	/// On average, each meshlet has >95% triangle occupancy
	/// </summary>
	/// <param name="mesh :">Source mesh</param>
	/// <returns>Returns the converted meshlet mesh</returns>
	MeshletMesh convertMeshToMeshletMeshSmartFast(Mesh& mesh);

	/// <summary>
	/// Convert a normal mesh into a meshlet mesh
	/// Use a "slow" smart scan which is slower then the linear one and the fast smart scan
	/// which produces the best meshlets.
	/// On average, each meshlet has >98% triangle occupancy
	/// </summary>
	/// <param name="mesh :">Source mesh</param>
	/// <returns>Returns the converted meshlet mesh</returns>
	MeshletMesh convertMeshToMeshletMeshSmartSlow(Mesh& mesh);
}