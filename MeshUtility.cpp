#include "MeshUtility.h"

#include "Mesh.h"
#include "VertexDeduplicationGrid.h"
#include "Frustum.h"



void MeshUtility::removeDuplicateVerticesByPosition(Mesh& mesh) {

	// An array of new indices is created because if several vertices are duplicates then the previous indices are invalid

	std::vector<int> newVertexIndices((int)mesh.vertices.size());
	std::fill(newVertexIndices.begin(), newVertexIndices.end(), -1);

	// For performance reasons a grid approach is used for duplicate searching

	// It requires a bounding box of the grid, a little expanded just in case

	BoundingBox boundingBox = { mesh.boundingSphere.position - mesh.boundingSphere.radius, mesh.boundingSphere.position + mesh.boundingSphere.radius };
	boundingBox.minimum -= 0.01f;
	boundingBox.maximum += 0.01f;

	// The number of cells is proportional to the number of vertices

	int cellCount = (int)cbrt(mesh.vertices.size());

	VertexDeduplicationGrid grid(boundingBox, cellCount);
	for (int i = 0; i < (int)mesh.vertices.size(); i++) {
		grid.insertGridPoint({ mesh.vertices[i].position, i });
	}

	std::vector<VertexCPU> uniqueVertices;
	std::vector<VDGPoint> duplicates;
	for (int i = 0; i < (int)mesh.vertices.size(); i++) {

		// If the new vertex index is not (-1) skip it since it has been removed

		if (newVertexIndices[i] != -1) {
			continue;
		}

		// Get all duplicates

		duplicates.clear();
		grid.getNeighboringGridPoints({ mesh.vertices[i].position, i }, duplicates);

		// Check all points for duplicates, if there are duplicates mark their indices

		for (int j = 0; j < (int)duplicates.size(); j++) {

			Vector3f point1 = mesh.vertices[i].position;
			Vector3f point2 = duplicates[j].position;

			if ((point1 - point2).magnitude() < FLT_EPSILON * 10) {
				newVertexIndices[duplicates[j].ID] = (int)uniqueVertices.size();
			}
		}

		// Add the new index to the array 

		newVertexIndices[i] = (int)uniqueVertices.size();
		uniqueVertices.push_back(mesh.vertices[i]);
	}

	// Map the old vertices to new ones

	std::for_each(mesh.indices.begin(), mesh.indices.end(), [newVertexIndices](int& index) { index = newVertexIndices[index]; });

	mesh.vertices = uniqueVertices;
}

void MeshUtility::updateBoundingBox(BoundingBox& boundingBox, Vector3f position) {

	for (int i = 0; i < 3; i++) {
		boundingBox.minimum.data[i] = std::min(boundingBox.minimum.data[i], position.data[i]);
		boundingBox.maximum.data[i] = std::max(boundingBox.maximum.data[i], position.data[i]);
	}
}

BoundingBox MeshUtility::calculateBoundingBox(std::vector<VertexCPU>& vertices) {

	BoundingBox boundingBox;
	boundingBox.minimum = Vector3f(FLT_MAX);
	boundingBox.maximum = Vector3f(-FLT_MAX);

	for (int i = 0; i < vertices.size(); i++) {
		MeshUtility::updateBoundingBox(boundingBox, vertices[i].position);
	}

	return boundingBox;
}

BoundingBox MeshUtility::calculateBoundingBox(std::vector<VertexCPU>& vertices, std::span<int> vertexIndices) {

	BoundingBox boundingBox;
	boundingBox.minimum = Vector3f(FLT_MAX);
	boundingBox.maximum = Vector3f(-FLT_MAX);

	for (int i = 0; i < vertexIndices.size(); i++) {
		MeshUtility::updateBoundingBox(boundingBox, vertices[vertexIndices[i]].position);
	}

	return boundingBox;
}

BoundingSphere MeshUtility::calculateBoundingSphere(std::vector<VertexCPU>& vertices) {

	BoundingSphere boundingSphere;

	Vector3d vertexSum;
	for (int i = 0; i < (int)vertices.size(); i++) {
		vertexSum += vertices[i].position;
	}

	boundingSphere.position = vertexSum / (int)vertices.size();

	for (int i = 0; i < (int)vertices.size(); i++) {
		boundingSphere.radius = std::max(boundingSphere.radius, (vertices[i].position - boundingSphere.position).magnitude());
	}

	return boundingSphere;
}

BoundingSphere MeshUtility::calculateBoundingSphere(std::vector<VertexCPU>& vertices, std::span<int> vertexIndices) {

	BoundingSphere boundingSphere;

	Vector3d vertexSum;
	for (int i = 0; i < (int)vertexIndices.size(); i++) {
		vertexSum += vertices[vertexIndices[i]].position;
	}

	boundingSphere.position = vertexSum / (int)vertexIndices.size();

	for (int i = 0; i < (int)vertexIndices.size(); i++) {
		boundingSphere.radius = std::max(boundingSphere.radius, (vertices[vertexIndices[i]].position - boundingSphere.position).magnitude());
	}

	return boundingSphere;
}

Vector3f MeshUtility::calculateMeshletNormal(MeshletData& meshletData, std::vector<VertexCPU>& vertices) {

	Vector3f normal;

	for (int i = 0; i < meshletData.primitiveCount; i++) {

		Vector3f vertex0 = vertices[meshletData.vertexIndices[meshletData.primitiveindices[3ull * i + 0ull]]].position;
		Vector3f vertex1 = vertices[meshletData.vertexIndices[meshletData.primitiveindices[3ull * i + 1ull]]].position;
		Vector3f vertex2 = vertices[meshletData.vertexIndices[meshletData.primitiveindices[3ull * i + 2ull]]].position;

		Vector3f vector1 = vertex1 - vertex0;
		Vector3f vector2 = vertex2 - vertex0;

		Vector3f crossProduct = GML::Cross(vector1, vector2);

		// If the new vector is of length 0 ignore it, not the best solution but it works

		if (crossProduct.magnitude() > 0) {
			normal += GML::Normalize(crossProduct);
		}
	}

	return GML::Normalize(normal);
}

bool MeshUtility::doTrianglesShareAnAdge(Vector3i triangle1, Vector3i triangle2) {

	int xCount = int(triangle1.x == triangle2.x) + int(triangle1.x == triangle2.y) + int(triangle1.x == triangle2.z);
	int yCount = int(triangle1.y == triangle2.x) + int(triangle1.y == triangle2.y) + int(triangle1.y == triangle2.z);
	int zCount = int(triangle1.z == triangle2.x) + int(triangle1.z == triangle2.y) + int(triangle1.z == triangle2.z);

	return (xCount + yCount + zCount) == 2;
}

bool MeshUtility::isVisibleInFrustum(Frustum& frustum, BoundingSphere& sphere) {

	bool visible = true;
	for (int i = 0; i < 6; i++) {

		float distance = GML::Dot(sphere.position - frustum.planes[i].position, frustum.planes[i].normal);

		if (distance > sphere.radius) {
			visible = false;
		}
	}

	return visible;
}

BoundingBox MeshUtility::convertBoundingSphereToBoundingBox(BoundingSphere boundingSphere) {

	return { boundingSphere.position - boundingSphere.radius, boundingSphere.position + boundingSphere.radius };
}

std::vector<VertexGPU> MeshUtility::convertCPUVerticesToGPUVertices(std::vector<VertexCPU>& vertices) {

	std::vector<VertexGPU> result(vertices.size());

	std::transform(vertices.begin(), vertices.end(), result.begin(), [](VertexCPU& vertex) -> VertexGPU {
		return { Vector4f(vertex.position, 1), Vector4f(vertex.normal, 0), Vector4f(vertex.uv, 0) };
		});

	return result;
}



