#pragma once

#include "Includes.h"

#include "Vector.h"



struct Mesh;
struct MeshletData;
struct BoundingBox;
struct BoundingSphere;
struct Frustum;
struct VertexCPU;
struct VertexGPU;



namespace MeshUtility {

	/// <summary>
	/// Remove duplicate vertices from a mesh based on their positions.
	/// Vertices are considered to be duplicates if the euclidean distance between them
	/// is less than (10 * FLT_EPSILON), any distance greater than that is 
	/// treated as if the vertices are not duplicates.
	/// </summary>
	/// <param name="mesh :">An existing mesh from which to remove duplicates</param>
	void removeDuplicateVerticesByPosition(Mesh& mesh);

	/// <summary>
	/// Update a bounding box with a new position
	/// </summary>
	/// <param name="boundingBox :">An existing bounding box</param>
	/// <param name="position :">A position to update the bounding box by</param>
	void updateBoundingBox(BoundingBox& boundingBox, Vector3f position);

	/// <summary>
	/// Calculate the bounding box of a set of vertices
	/// </summary>
	/// <param name="vertices :">Array of vertices</param>
	/// <returns>The returned value is the calculated bounding box</returns>
	BoundingBox calculateBoundingBox(std::vector<VertexCPU>& vertices);

	/// <summary>
	/// Calculate the bounding box of a set of vertices using the provided indices
	/// </summary>
	/// <param name="vertices :">Array of vertices</param>
	/// <param name="vertexIndices :">Array of vertex indices</param>
	/// <returns>The returned value is the calculated bounding box</returns>
	BoundingBox calculateBoundingBox(std::vector<VertexCPU>& vertices, std::span<int> vertexIndices);

	/// <summary>
	/// Calculate the bounding sphere of a set of vertices
	/// </summary>
	/// <param name="vertices :">Array of vertices</param>
	/// <returns>The returned value is the calculated bounding sphere</returns>
	BoundingSphere calculateBoundingSphere(std::vector<VertexCPU>& vertices);

	/// <summary>
	/// Calculate the bounding sphere of a set of vertices
	/// </summary>
	/// <param name="vertices :">Array of vertices</param>
	/// <returns>The returned value is the calculated bounding sphere</returns>
	BoundingSphere calculateBoundingSphere(std::vector<VertexCPU>& vertices, std::span<int> vertexIndices);

	/// <summary>
	/// Calculate the normal of a meshlet
	/// </summary>
	/// <param name="meshletData :">Meshlet used to calculate its normal</param>
	/// <param name="vertices :">Vertex array the meshlet is indexing</param>
	/// <returns>Meshlet normal</returns>
	Vector3f calculateMeshletNormal(MeshletData& meshletData, std::vector<VertexCPU>& vertices);

	/// <summary>
	/// Check if two triangles given by indices share an adge
	/// </summary>
	/// <param name="triangle1 :">Indices of the first triangle</param>
	/// <param name="triangle2 :">Indices if the secondd triangle</param>
	/// <returns>Return (true) if two indices in both triangles match, otherwise return (false)</returns>
	bool doTrianglesShareAnAdge(Vector3i triangle1, Vector3i triangle2);

	/// <summary>
	/// Check if a bounding sphere is visible in the view frustum
	/// </summary>
	/// <param name="frustum :">Frustum to check</param>
	/// <param name="sphere :">Bounding sphere to check</param>
	/// <returns>Return (true/false) based on visiblity</returns>
	bool isVisibleInFrustum(Frustum& frustum, BoundingSphere& sphere);

	/// <summary>
	/// Convert a bounding sphere to a bounding box
	/// </summary>
	/// <param name="boundingSphere :">Bounding sphere</param>
	/// <returns>Return a bounding box</returns>
	BoundingBox convertBoundingSphereToBoundingBox(BoundingSphere boundingSphere);

	/// <summary>
	/// Convert a vector of CPU vertices to a vector of GPU vertices
	/// </summary>
	/// <param name="vertices :">Vector of CPU vertices</param>
	/// <returns>Return a vector of GPU vertices</returns>
	std::vector<VertexGPU> convertCPUVerticesToGPUVertices(std::vector<VertexCPU>& vertices);
}
