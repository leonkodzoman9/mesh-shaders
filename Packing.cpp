#include "Packing.h"



int8_t Packing::floatToSNorm8(float value) {

	return (int8_t)std::round(std::clamp(value, -1.0f, 1.0f) * 127.0f);
}
uint8_t Packing::floatToUNorm8(float value) {

	return (uint8_t)std::round(std::clamp(value, 0.0f, 1.0f) * 255.0f);
}

int16_t Packing::floatToSNorm16(float value) {

	return (int16_t)std::round(std::clamp(value, -1.0f, 1.0f) * 32767.0f);
}
uint16_t Packing::floatToUNorm16(float value) {

	return (uint16_t)std::round(std::clamp(value, 0.0f, 1.0f) * 65535.0f);
}



