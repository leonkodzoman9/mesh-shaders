#pragma once

#include "Includes.h"



namespace Packing {

	int8_t floatToSNorm8(float value);
	uint8_t floatToUNorm8(float value);

	int16_t floatToSNorm16(float value);
	uint16_t floatToUNorm16(float value);
}



