#include "PrimitiveMeshes.h"

#include "Mesh.h"
#include "MeshUtility.h"



void subdivideTriangles(Mesh& mesh, int subdivisions) {

	Mesh temporary = Primitive::generateIcosahedron();

	int verticesPerTriangle = (1 + (subdivisions + 1)) * (subdivisions + 1) / 2;
	mesh.vertices.resize(20ull * verticesPerTriangle);

	// Loop over every triangle in the icosahedron
	int vertexIndex = 0;
	for (int triangle = 0; triangle < 20; triangle++) {

		Vector3f point1 = temporary.vertices[temporary.indices[3 * triangle + 0]].position;
		Vector3f point2 = temporary.vertices[temporary.indices[3 * triangle + 1]].position;
		Vector3f point3 = temporary.vertices[temporary.indices[3 * triangle + 2]].position;

		Vector3f vector1 = (point2 - point1) / (float)subdivisions;
		Vector3f vector2 = (point3 - point1) / (float)subdivisions;

		// Generate vertices from each triangle that will make up the icosphere
		for (int i = 0; i < subdivisions + 1; i++) {
			for (int j = 0; j < subdivisions + 1 - i; j++) {

				Vector3f vertex = GML::Normalize(point1 + vector1 * (float)j + vector2 * (float)i);

				mesh.vertices[vertexIndex++] = { vertex, vertex, vertex };
			}
		}
	}
}
void generateIndices(Mesh& mesh, int subdivisions) {

	// Vertical offsets, makes sense on paper, eg.
	// (9)
	// (7) 8
	// (4) 5 6
	// (0) 2 3 4
	std::vector<int> offsets(subdivisions + 1ull);
	offsets[0] = 0;
	for (int i = 0; i < subdivisions; i++) {
		offsets[i + 1ull] = offsets[i] + subdivisions + 1 - i;
	}

	mesh.indices.resize(60ull * subdivisions * subdivisions);

	// How many vertices there are per triangle
	int vertexCount = (subdivisions + 1) * (subdivisions + 2) / 2;

	// Generate the same set of indices 20 times but with different offsets
	int indexIndex = 0;
	for (int triangle = 0; triangle < 20; triangle++) {

		for (int i = 0; i < subdivisions; i++) {
			for (int j = 0; j < subdivisions - 1 - i; j++) {

				int bl = j + offsets[i];
				int br = bl + 1;
				int tl = j + offsets[i + 1ull];
				int tr = tl + 1;

				mesh.indices[indexIndex++] = bl + vertexCount * triangle;
				mesh.indices[indexIndex++] = br + vertexCount * triangle;
				mesh.indices[indexIndex++] = tl + vertexCount * triangle;
				mesh.indices[indexIndex++] = br + vertexCount * triangle;
				mesh.indices[indexIndex++] = tr + vertexCount * triangle;
				mesh.indices[indexIndex++] = tl + vertexCount * triangle;
			}

			int bl = subdivisions - 1 - i + offsets[i];
			int br = bl + 1;
			int tl = subdivisions - 1 - i + offsets[i + 1ull];

			mesh.indices[indexIndex++] = bl + vertexCount * triangle;
			mesh.indices[indexIndex++] = br + vertexCount * triangle;
			mesh.indices[indexIndex++] = tl + vertexCount * triangle;
		}
	}
}

namespace Primitive {

	Mesh generateCube() {

		Mesh mesh;

		mesh.vertices.push_back({ Vector3f(-1,-1, 1), Vector3f::Front(), Vector3f(0, 0, 0) });
		mesh.vertices.push_back({ Vector3f(1,-1, 1), Vector3f::Front(), Vector3f(0, 1, 0) });
		mesh.vertices.push_back({ Vector3f(1, 1, 1), Vector3f::Front(), Vector3f(1, 1, 0) });
		mesh.vertices.push_back({ Vector3f(-1, 1, 1), Vector3f::Front(), Vector3f(1, 0, 0) });

		mesh.vertices.push_back({ Vector3f(1,-1, 1), Vector3f::Right(), Vector3f(0, 0, 0) });
		mesh.vertices.push_back({ Vector3f(1,-1,-1), Vector3f::Right(), Vector3f(0, 1, 0) });
		mesh.vertices.push_back({ Vector3f(1, 1,-1), Vector3f::Right(), Vector3f(1, 1, 0) });
		mesh.vertices.push_back({ Vector3f(1, 1, 1), Vector3f::Right(), Vector3f(1, 0, 0) });

		mesh.vertices.push_back({ Vector3f(1,-1,-1), Vector3f::Back(), Vector3f(0, 0, 0) });
		mesh.vertices.push_back({ Vector3f(-1,-1,-1), Vector3f::Back(), Vector3f(0, 1, 0) });
		mesh.vertices.push_back({ Vector3f(-1, 1,-1), Vector3f::Back(), Vector3f(1, 1, 0) });
		mesh.vertices.push_back({ Vector3f(1, 1,-1), Vector3f::Back(), Vector3f(1, 0, 0) });

		mesh.vertices.push_back({ Vector3f(-1,-1,-1), Vector3f::Left(), Vector3f(0, 0, 0) });
		mesh.vertices.push_back({ Vector3f(-1,-1, 1), Vector3f::Left(), Vector3f(0, 1, 0) });
		mesh.vertices.push_back({ Vector3f(-1, 1, 1), Vector3f::Left(), Vector3f(1, 1, 0) });
		mesh.vertices.push_back({ Vector3f(-1, 1,-1), Vector3f::Left(), Vector3f(1, 0, 0) });

		mesh.vertices.push_back({ Vector3f(-1, 1, 1), Vector3f::Up(), Vector3f(0, 0, 0) });
		mesh.vertices.push_back({ Vector3f(1, 1, 1), Vector3f::Up(), Vector3f(0, 1, 0) });
		mesh.vertices.push_back({ Vector3f(1, 1,-1), Vector3f::Up(), Vector3f(1, 1, 0) });
		mesh.vertices.push_back({ Vector3f(-1, 1,-1), Vector3f::Up(), Vector3f(1, 0, 0) });

		mesh.vertices.push_back({ Vector3f(-1,-1, 1), Vector3f::Down(), Vector3f(0, 0, 0) });
		mesh.vertices.push_back({ Vector3f(-1,-1,-1), Vector3f::Down(), Vector3f(0, 1, 0) });
		mesh.vertices.push_back({ Vector3f(1,-1,-1), Vector3f::Down(), Vector3f(1, 1, 0) });
		mesh.vertices.push_back({ Vector3f(1,-1, 1), Vector3f::Down(), Vector3f(1, 0, 0) });

		mesh.indices.insert(mesh.indices.end(), { 0 +  0, 1 +  0, 2 +  0 });
		mesh.indices.insert(mesh.indices.end(), { 0 +  0, 2 +  0, 3 +  0 });
		mesh.indices.insert(mesh.indices.end(), { 0 +  4, 1 +  4, 2 +  4 });
		mesh.indices.insert(mesh.indices.end(), { 0 +  4, 2 +  4, 3 +  4 });
		mesh.indices.insert(mesh.indices.end(), { 0 +  8, 1 +  8, 2 +  8 });
		mesh.indices.insert(mesh.indices.end(), { 0 +  8, 2 +  8, 3 +  8 });
		mesh.indices.insert(mesh.indices.end(), { 0 + 12, 1 + 12, 2 + 12 });
		mesh.indices.insert(mesh.indices.end(), { 0 + 12, 2 + 12, 3 + 12 });
		mesh.indices.insert(mesh.indices.end(), { 0 + 16, 1 + 16, 2 + 16 });
		mesh.indices.insert(mesh.indices.end(), { 0 + 16, 2 + 16, 3 + 16 });
		mesh.indices.insert(mesh.indices.end(), { 0 + 20, 1 + 20, 2 + 20 });
		mesh.indices.insert(mesh.indices.end(), { 0 + 20, 2 + 20, 3 + 20 });

		std::for_each(mesh.vertices.begin(), mesh.vertices.end(), [](VertexCPU& vertex) { vertex.position *= 0.5; });

		mesh.boundingSphere = MeshUtility::calculateBoundingSphere(mesh.vertices);

		return mesh;
	}

	Mesh generateUVSphere(int subdivisions) {

		Mesh mesh;

		mesh.vertices.resize((subdivisions + 1ull) * (subdivisions + 1ull));
		mesh.indices.resize((uint64_t)(subdivisions) * (uint64_t)(subdivisions) * 6ull);

		float ak = std::numbers::pi_v<float> * 2 / (float)subdivisions;

		for (int i = 0; i < subdivisions + 1; i++) {
			for (int j = 0; j < subdivisions + 1; j++) {

				float yaw = ak * (float)i;
				float pitch = ak * (float)j / 2.0f;

				Vector3f position = GML::FromPolar(1.0f, yaw, pitch);

				mesh.vertices[i * (subdivisions + 1ull) + j] = { position , position, Vector3f((float)i / subdivisions, 1 - (float)j / subdivisions, 0.0f) };
			}
		}

		for (int i = 0; i < subdivisions; i++) {
			for (int j = 0; j < subdivisions; j++) {

				int i0 = (subdivisions + 1) * i + j + subdivisions + 1;
				int i1 = (subdivisions + 1) * i + j + subdivisions + 1 + 1;
				int i2 = (subdivisions + 1) * i + j + 1;
				int i3 = (subdivisions + 1) * i + j;

				mesh.indices[6ull * (i * (uint64_t)(subdivisions)+j) + 0] = i0;
				mesh.indices[6ull * (i * (uint64_t)(subdivisions)+j) + 1] = i2;
				mesh.indices[6ull * (i * (uint64_t)(subdivisions)+j) + 2] = i1;
				mesh.indices[6ull * (i * (uint64_t)(subdivisions)+j) + 3] = i0;
				mesh.indices[6ull * (i * (uint64_t)(subdivisions)+j) + 4] = i3;
				mesh.indices[6ull * (i * (uint64_t)(subdivisions)+j) + 5] = i2;
			}
		}

		mesh.boundingSphere = MeshUtility::calculateBoundingSphere(mesh.vertices);

		return mesh;
	}

	Mesh generateIcoSphere(int subdivisions) {

		Mesh mesh;

		subdivideTriangles(mesh, subdivisions);
		generateIndices(mesh, subdivisions);

		mesh.boundingSphere = MeshUtility::calculateBoundingSphere(mesh.vertices);

		MeshUtility::removeDuplicateVerticesByPosition(mesh);

		return mesh;
	}

	Mesh generateTorus(int subdivisions) {

		Mesh mesh;

		float ak = std::numbers::pi_v<float> * 2 / (float)subdivisions;

		for (int i = 0; i < subdivisions * 2 + 1; i++) {
			for (int j = 0; j < subdivisions + 1; j++) {

				float yaw = ak * (float)i / 2;
				float pitch = ak * (float)j;

				Vector3f radius = GML::FromPolar(1.0f, yaw, std::numbers::pi_v<float> / 2);
				Vector3f perimeter = GML::FromPolar(1.0f, yaw, pitch);

				mesh.vertices.push_back({ radius + perimeter * 0.25 , perimeter,Vector3f((float)i / (subdivisions * 2), 1 - (float)j / subdivisions, 0) });
			}
		}

		for (int i = 0; i < subdivisions * 2; i++) {
			for (int j = 0; j < subdivisions; j++) {

				int i0 = (subdivisions + 1) * i + j + subdivisions + 1;
				int i1 = (subdivisions + 1) * i + j + subdivisions + 1 + 1;
				int i2 = (subdivisions + 1) * i + j + 1;
				int i3 = (subdivisions + 1) * i + j;

				mesh.indices.insert(mesh.indices.end(), { i0, i2, i1 });
				mesh.indices.insert(mesh.indices.end(), { i0, i3, i2 });
			}
		}

		mesh.boundingSphere = MeshUtility::calculateBoundingSphere(mesh.vertices);

		return mesh;
	}

	Mesh generateIcosahedron() {

		Mesh mesh;

		float phi = (1.0f + sqrtf(5.0f)) / 2.0f;

		Vector3f vertexTable[12] = {
			GML::Normalize(Vector3f(-0.5f, 0.0f, phi / 2.0f)),
			GML::Normalize(Vector3f(0.5f, 0.0f, phi / 2.0f)),
			GML::Normalize(Vector3f(0.5f, 0.0f, -phi / 2.0f)),
			GML::Normalize(Vector3f(-0.5f, 0.0f, -phi / 2.0f)),

			GML::Normalize(Vector3f(0.0f, -phi / 2.0f, 0.5f)),
			GML::Normalize(Vector3f(0.0f, -phi / 2.0f, -0.5f)),
			GML::Normalize(Vector3f(0.0f, phi / 2.0f, -0.5f)),
			GML::Normalize(Vector3f(0.0f, phi / 2.0f, 0.5f)),

			GML::Normalize(Vector3f(-phi / 2.0f, -0.5f, 0.0f)),
			GML::Normalize(Vector3f(phi / 2.0f, -0.5f, 0.0f)),
			GML::Normalize(Vector3f(phi / 2.0f, 0.5f, 0.0f)),
			GML::Normalize(Vector3f(-phi / 2.0f, 0.5f, 0.0f))
		};

		std::array<Vector3f, 60> vertices = {
			vertexTable[0], vertexTable[1], vertexTable[7],
			vertexTable[0], vertexTable[7], vertexTable[11],
			vertexTable[0], vertexTable[11], vertexTable[8],
			vertexTable[0], vertexTable[8], vertexTable[4],
			vertexTable[0], vertexTable[4], vertexTable[1],

			vertexTable[2], vertexTable[3], vertexTable[6],
			vertexTable[2], vertexTable[6], vertexTable[10],
			vertexTable[2], vertexTable[10], vertexTable[9],
			vertexTable[2], vertexTable[9], vertexTable[5],
			vertexTable[2], vertexTable[5], vertexTable[3],

			vertexTable[1], vertexTable[9], vertexTable[10],
			vertexTable[1], vertexTable[10], vertexTable[7],

			vertexTable[7], vertexTable[10], vertexTable[6],
			vertexTable[7], vertexTable[6], vertexTable[11],

			vertexTable[11], vertexTable[6], vertexTable[3],
			vertexTable[11], vertexTable[3], vertexTable[8],

			vertexTable[8], vertexTable[3], vertexTable[5],
			vertexTable[8], vertexTable[5], vertexTable[4],

			vertexTable[4], vertexTable[5], vertexTable[9],
			vertexTable[4], vertexTable[9], vertexTable[1],
		};

		for (int i = 0; i < 60; i += 3) {
			mesh.vertices.push_back({ vertices[i + 0ull], Vector3f(1), Vector3f(1) });
			mesh.vertices.push_back({ vertices[i + 1ull], Vector3f(1), Vector3f(1) });
			mesh.vertices.push_back({ vertices[i + 2ull], Vector3f(1), Vector3f(1) });
		}

		for (int i = 0; i < 60; i += 3) {

			Vector3f normal = (mesh.vertices[i + 0ull].position + mesh.vertices[i + 1ull].position + mesh.vertices[i + 2ull].position) / 3;

			mesh.vertices[i + 0ull].normal = normal;
			mesh.vertices[i + 1ull].normal = normal;
			mesh.vertices[i + 2ull].normal = normal;

			mesh.vertices[i + 0ull].uv = mesh.vertices[i + 0ull].normal;
			mesh.vertices[i + 1ull].uv = mesh.vertices[i + 1ull].normal;
			mesh.vertices[i + 2ull].uv = mesh.vertices[i + 2ull].normal;
		}

		for (int i = 0; i < 20; i++) {
			mesh.indices.insert(mesh.indices.end(), { 3 * i + 0, 3 * i + 1, 3 * i + 2 });
		}

		mesh.boundingSphere = MeshUtility::calculateBoundingSphere(mesh.vertices);

		return mesh;
	}
}