#pragma once

#include "Includes.h"



struct Mesh;



namespace Primitive {

	Mesh generateCube();

	Mesh generateUVSphere(int subdivisions = 32);

	Mesh generateIcoSphere(int subdivisions = 8);

	Mesh generateTorus(int subdivisions = 32);

	Mesh generateIcosahedron();
}
