#include "Includes.h"

#include "Window.h"
#include "Camera.h"
#include "Mouse.h"
#include "Keyboard.h"

#include "BufferGL.h"
#include "BufferDescriptorGL.h"
#include "ShaderProgramGL.h"

#include "Timer.h"
#include "Frustum.h"

#include "Mesh.h"
#include "MeshUtility.h"
#include "MeshConversion.h"
#include "PrimitiveMeshes.h"



enum class DrawMode {
	NORMAL,
	MESH
};



int main() {

	Window::getInstance().initialize(Vector2i(1600, 900));
	glfwSwapInterval(0);



	Timer timer;

	// Change type of object
	Mesh mesh = Primitive::generateUVSphere(6144);

	timer.update();
	printf("Mesh creation : %f ms\n", timer.getInterval());

	std::for_each(std::execution::par_unseq, mesh.vertices.begin(), mesh.vertices.end(), [](VertexCPU& vertex) { vertex.position *= 50; });
	mesh.boundingSphere.radius *= 50;

	timer.update();
	printf("Mesh scaling : %f ms\n", timer.getInterval());

	printf("Vertices : %lld\n", mesh.vertices.size());
	printf("Triangles : %lld\n", mesh.indices.size() / 3);

	// Change meshlet generation algorithm
	MeshletMesh meshletMesh = MeshConversion::convertMeshToMeshletMeshLinear(mesh);

	printf("Meshlets : %lld\n", meshletMesh.meshlets.size());

	timer.update();
	printf("Mesh conversion to meshlets : %f ms\n", timer.getInterval());

	double verticesPerMeshlet = 0;
	double trianglesPerMeshlet = 0;
	for (int i = 0; i < meshletMesh.meshlets.size(); i++) {
		verticesPerMeshlet += meshletMesh.meshlets[i].vertexIndexCount;
		trianglesPerMeshlet += meshletMesh.meshlets[i].primitiveCount;
	}

	verticesPerMeshlet /= meshletMesh.meshlets.size();
	trianglesPerMeshlet /= meshletMesh.meshlets.size();

	printf("Utilization Vertices/Meshlet : %f, %f %%\n", verticesPerMeshlet, verticesPerMeshlet * 100.0f / 64);
	printf("Utilization Triangles/Meshlet : %f, %f %%\n\n", trianglesPerMeshlet, trianglesPerMeshlet * 100.0f / 84);



	std::vector<VertexGPU> vertices = MeshUtility::convertCPUVerticesToGPUVertices(mesh.vertices);

	BufferGL vertexBuffer, indexBuffer;
	vertexBuffer.setData(vertices.data(), vertices.size() * sizeof(VertexGPU), GL_STATIC_DRAW);
	indexBuffer.setData(mesh.indices.data(), mesh.indices.size() * sizeof(int), GL_STATIC_DRAW);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, vertexBuffer.ID);

	BufferGL vertexIndexBuffer, meshletIndexBuffer, meshletBuffer;
	vertexIndexBuffer.setData(meshletMesh.vertexIndices.data(), meshletMesh.vertexIndices.size() * sizeof(int), GL_STATIC_DRAW);
	meshletIndexBuffer.setData(meshletMesh.meshletIndices.data(), meshletMesh.meshletIndices.size() * sizeof(uint8_t), GL_STATIC_DRAW);
	meshletBuffer.setData(meshletMesh.meshlets.data(), meshletMesh.meshlets.size() * sizeof(Meshlet), GL_STATIC_DRAW);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, vertexIndexBuffer.ID);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, meshletIndexBuffer.ID);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 3, meshletBuffer.ID);



	int64_t vertexBufferSize = vertices.size() * sizeof(VertexGPU);
	int64_t indexBufferSize = mesh.indices.size() * sizeof(int);
	int64_t vertexIndexBufferSize = meshletMesh.vertexIndices.size() * sizeof(int);
	int64_t meshletIndexBufferSize = meshletMesh.meshletIndices.size() * sizeof(uint8_t);
	int64_t meshletBufferSize = meshletMesh.meshlets.size() * sizeof(Meshlet);

	printf("Vertex buffer size : %f MB\n", vertexBufferSize / (1024.0f * 1024.0f));
	printf("Index buffer size : %f MB\n\n", indexBufferSize / (1024.0f * 1024.0f));

	printf("Vertex index buffer size : %f MB\n", vertexIndexBufferSize / (1024.0f * 1024.0f));
	printf("Meshlet index buffer size : %f MB\n", meshletIndexBufferSize / (1024.0f * 1024.0f));
	printf("Meshlet buffer size : %f MB\n\n", meshletBufferSize / (1024.0f * 1024.0f));

	printf("Standard shaders memory usage : %f MB\n", (vertexBufferSize + indexBufferSize) / (1024.0f * 1024.0f));
	printf("Mesh shaders memory usage : %f MB\n\n", (vertexBufferSize + vertexIndexBufferSize + meshletIndexBufferSize + meshletBufferSize) / (1024.0f * 1024.0f));

	printf("Total memory usage : %f MB\n", (vertexBufferSize + indexBufferSize + vertexIndexBufferSize + meshletIndexBufferSize + meshletBufferSize) / (1024.0f * 1024.0f));


	BufferDescriptorGL descriptor;
	
	BDLayoutGL segment;
	segment.bindingPoint = 0;
	segment.bufferOffset = 0;
	segment.bufferStride = sizeof(Vector4f) * 3;
	segment.dataSource = &vertexBuffer;
	segment.layout = BufferDescriptorLayout::Layout::FLOAT4;
	descriptor.setLayout({ segment }, indexBuffer);
	


	ShaderProgramGL shaderNormal;
	shaderNormal.addShader("shaderSimple.vert", GL_VERTEX_SHADER);
	shaderNormal.addShader("shaderSimple.frag", GL_FRAGMENT_SHADER);
	shaderNormal.createProgram();
	
	ShaderProgramGL shaderMesh;
	shaderMesh.addShader("shaderSimpleMesh.task", GL_TASK_SHADER_NV);
	shaderMesh.addShader("shaderSimpleMesh.mesh", GL_MESH_SHADER_NV);
	shaderMesh.addShader("shaderSimpleMesh.frag", GL_FRAGMENT_SHADER);
	shaderMesh.createProgram();
	


	Camera camera(Vector3f(-mesh.boundingSphere.radius - 10, 0, 0), 0, 0, 90, 0.01, 500);
	
	DrawMode drawMode = DrawMode::NORMAL;
	bool firstFrame = true;

	bool cursorVisible = true;

	int timeIndex = 0;
	std::vector<double> times(100);

	while (!glfwWindowShouldClose(Window::getInstance().getWindow())) {

		Window::getInstance().setTitle(std::to_string(std::reduce(times.begin(), times.end(), 0.0) / 100.0));

		glfwPollEvents();

		Mouse::getInstance().update();
		Keyboard::getInstance().update();
		



		if (firstFrame) {
			firstFrame = false;
			continue;
		}

		int64_t timeStart;
		glGetInteger64v(GL_TIMESTAMP, &timeStart);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		Matrix4f viewProjection = camera.getProjectionMatrix(16 / 9.0) * camera.getViewMatrix();
		Matrix4f model = GML::Rotation(Vector3f(0, glfwGetTime() / 100, 0));

		if (drawMode == DrawMode::NORMAL) {

			glEnable(GL_DEPTH_TEST);
			glEnable(GL_CULL_FACE);

			shaderNormal.use();
			shaderNormal.setMatrix4f("viewProjection", viewProjection, true);
			shaderNormal.setMatrix4f("model", model, true);
			shaderNormal.setVector4f("color", Vector4f(1));

			glBindVertexArray(descriptor.ID);
			glDrawElements(GL_TRIANGLES, mesh.indices.size(), GL_UNSIGNED_INT, 0);

			glDisable(GL_DEPTH_TEST);
			glDisable(GL_CULL_FACE);
		}

		if (drawMode == DrawMode::MESH) {

			glEnable(GL_DEPTH_TEST);

			BoundingBox meshletMeshBoundingBox = MeshUtility::convertBoundingSphereToBoundingBox(meshletMesh.boundingSphere);

			shaderMesh.use();
			shaderMesh.setMatrix4f("viewProjection", viewProjection, true);
			shaderMesh.setMatrix4f("model", model, true);
			shaderMesh.setVector3f("meshBBMins", meshletMeshBoundingBox.minimum);
			shaderMesh.setVector3f("meshBBMaxs", meshletMeshBoundingBox.maximum);
			shaderMesh.setVector3f("cameraPos", camera.position);
			shaderMesh.setInt("meshletCount", meshletMesh.meshlets.size());

			glDrawMeshTasksNV(0, meshletMesh.meshlets.size() / 32 + 1);

			glDisable(GL_DEPTH_TEST);
		}

		glfwSwapBuffers(Window::getInstance().getWindow());

		int64_t timeEnd;
		glGetInteger64v(GL_TIMESTAMP, &timeEnd);



		times[timeIndex] = (timeEnd - timeStart) / 1000000.0;
		timeIndex = (timeIndex + 1) % times.size();

		{
			Mouse& mouse = Mouse::getInstance();
			Keyboard& keyboard = Keyboard::getInstance();

			float mk = 1;
			if (keyboard.held(KeyCode::LEFT_CONTROL)) { mk = 5; }
			if (keyboard.held(KeyCode::LEFT_ALT)) { mk = 0.1; }

			if (keyboard.pressed(KeyCode::C)) {
				cursorVisible = !cursorVisible;
				if (cursorVisible) {
					glfwSetInputMode(Window::getInstance().getWindow(), GLFW_CURSOR, GLFW_CURSOR_NORMAL);
				}
				else {
					glfwSetInputMode(Window::getInstance().getWindow(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);
				}
			}

			if (!cursorVisible) {
				if (keyboard.held(KeyCode::W)) { camera.move(camera.front, 0.01 * mk); }
				if (keyboard.held(KeyCode::A)) { camera.move(-camera.right, 0.01 * mk); }
				if (keyboard.held(KeyCode::S)) { camera.move(-camera.front, 0.01 * mk); }
				if (keyboard.held(KeyCode::D)) { camera.move(camera.right, 0.01 * mk); }
				if (keyboard.held(KeyCode::SPACE)) { camera.move(Vector3f::Up(), 0.01 * mk); }
				if (keyboard.held(KeyCode::LEFT_SHIFT)) { camera.move(Vector3f::Down(), 0.01 * mk); }

				camera.rotate(mouse.deltaPosition(), 0.1);
			}

			if (keyboard.pressed(KeyCode::TAB)) { drawMode = drawMode == DrawMode::NORMAL ? DrawMode::MESH : DrawMode::NORMAL; }
			if (keyboard.held(KeyCode::LEFT_CONTROL) && keyboard.pressed(KeyCode::Q)) { break; }
		}
	}

	glfwTerminate();

	return 0;
}