#pragma once

#include "Includes.h"



class Timer {

protected:

    std::chrono::steady_clock::time_point startPoint;
    std::chrono::steady_clock::time_point stopPoint;

public:

    Timer();

    void start();
    void stop();
    void update();

    float getInterval() const;
};



class MultiTimer : public Timer {

private:

    std::vector<float> intervals;
    int currentIndex;
    int intervalCount;

public:

    MultiTimer(int intervalCount = 20);

    void update();

    float getAverageInterval() const;
};


