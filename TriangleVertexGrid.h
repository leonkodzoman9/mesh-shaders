#pragma once

#include "Includes.h"

#include "Vector.h"
#include "BoundingVolume.h"



// Special point used only by a vertex deduplication grid
struct TVGPoint {
	Vector3f position;
	int ID;
};

// A single cell of a vertex deduplication grid
struct TVGCell {

	std::vector<TVGPoint> points;
};

class TriangleVertexGrid {

private:

	BoundingBox boundingBox;
	int cellsPerSide;

	std::vector<TVGCell> vertexGrid;

public:

	TriangleVertexGrid();
	TriangleVertexGrid(BoundingBox boundingBox, int cellsPerSide);

	void insertGridPoint(TVGPoint gridPoint);

	void getNeighboringGridPoints(TVGPoint gridPoint, std::vector<TVGPoint>& neighboringPoints) const;
};