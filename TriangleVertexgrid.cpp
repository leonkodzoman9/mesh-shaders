#include "TriangleVertexGrid.h"




TriangleVertexGrid::TriangleVertexGrid() {

	this->cellsPerSide = 0;
}
TriangleVertexGrid::TriangleVertexGrid(BoundingBox boundingBox, int cellsPerSide) {

	this->boundingBox = boundingBox;
	this->cellsPerSide = cellsPerSide;

	int cellCount = cellsPerSide * cellsPerSide * cellsPerSide;

	this->vertexGrid.resize(cellCount);
	for (int i = 0; i < cellCount; i++) {
		this->vertexGrid.reserve(10);
	}
}

void TriangleVertexGrid::insertGridPoint(TVGPoint gridPoint) {

	Vector3f point = gridPoint.position - this->boundingBox.minimum;
	point /= (this->boundingBox.maximum - this->boundingBox.minimum);
	point *= (float)this->cellsPerSide;

	Vector3i indices = point;

	int cellIndex = indices.z * (this->cellsPerSide * this->cellsPerSide) + indices.y * this->cellsPerSide + indices.x;

	this->vertexGrid[cellIndex].points.push_back(gridPoint);
}

void TriangleVertexGrid::getNeighboringGridPoints(TVGPoint gridPoint, std::vector<TVGPoint>& neighboringPoints) const {

	Vector3f point = gridPoint.position - this->boundingBox.minimum;
	point /= (this->boundingBox.maximum - this->boundingBox.minimum);
	point *= (float)this->cellsPerSide;

	Vector3i indices = point;

	int index = indices.z * (this->cellsPerSide * this->cellsPerSide) + indices.y * this->cellsPerSide + indices.x;

	for (int n = 0; n < this->vertexGrid[index].points.size(); n++) {

		const TVGPoint& newPoint = this->vertexGrid[index].points[n];

		if (newPoint.ID == gridPoint.ID) {
			continue;
		}

		if (std::find_if(neighboringPoints.begin(), neighboringPoints.end(), [&](TVGPoint& point) { return point.ID == this->vertexGrid[index].points[n].ID; }) != neighboringPoints.end()) {
			continue;
		}

		neighboringPoints.push_back(this->vertexGrid[index].points[n]);
	}
}