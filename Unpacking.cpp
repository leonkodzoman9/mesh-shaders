#include "Unpacking.h"



float Unpacking::sNorm8ToFloat(int8_t value) {

	return value / 127.0f;
}
float Unpacking::uNorm8ToFloat(uint8_t value) {

	return value / 255.0f;
}
float Unpacking::sNorm16ToFloat(int16_t value) {

	return value / 32767.0f;
}
float Unpacking::uNorm16ToFloat(uint16_t value) {

	return value / 65535.0f;
}



