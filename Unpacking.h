#pragma once

#include "Includes.h"



namespace Unpacking {

	float sNorm8ToFloat(int8_t value);
	float uNorm8ToFloat(uint8_t value);

	float sNorm16ToFloat(int16_t value);
	float uNorm16ToFloat(uint16_t value);
}





