#include "VertexDeduplicationGrid.h"




VertexDeduplicationGrid::VertexDeduplicationGrid() {

	this->cellsPerSide = 0;
}
VertexDeduplicationGrid::VertexDeduplicationGrid(BoundingBox boundingBox, int cellsPerSide) {

	this->boundingBox = boundingBox;
	this->cellsPerSide = cellsPerSide;

	int cellCount = cellsPerSide * cellsPerSide * cellsPerSide;

	this->vertexGrid.resize(cellCount);
	for (int i = 0; i < cellCount; i++) {
		this->vertexGrid.reserve(10);
	}
}

void VertexDeduplicationGrid::insertGridPoint(VDGPoint gridPoint) {

	// Calculate where in the grid the point is and add it there

	Vector3f point = gridPoint.position - this->boundingBox.minimum;
	point /= (this->boundingBox.maximum - this->boundingBox.minimum);
	point *= (float)this->cellsPerSide;

	Vector3i indices = point;

	int cellIndex = indices.z * (this->cellsPerSide * this->cellsPerSide) + indices.y * this->cellsPerSide + indices.x;

	this->vertexGrid[cellIndex].points.push_back(gridPoint);
}

static std::array<Vector3f, 8> offsets = {
	Vector3f( FLT_EPSILON,  FLT_EPSILON,  FLT_EPSILON) * 2 ,
	Vector3f( FLT_EPSILON,  FLT_EPSILON, -FLT_EPSILON) * 2 ,
	Vector3f( FLT_EPSILON, -FLT_EPSILON,  FLT_EPSILON) * 2 ,
	Vector3f( FLT_EPSILON, -FLT_EPSILON, -FLT_EPSILON) * 2 ,
	Vector3f(-FLT_EPSILON,  FLT_EPSILON,  FLT_EPSILON) * 2 ,
	Vector3f(-FLT_EPSILON,  FLT_EPSILON, -FLT_EPSILON) * 2 ,
	Vector3f(-FLT_EPSILON, -FLT_EPSILON,  FLT_EPSILON) * 2 ,
	Vector3f(-FLT_EPSILON, -FLT_EPSILON, -FLT_EPSILON) * 2
};

void VertexDeduplicationGrid::getNeighboringGridPoints(VDGPoint gridPoint, std::vector<VDGPoint>& neighboringPoints) {

	// Calculate where in the grid the point is

	Vector3f point = gridPoint.position - this->boundingBox.minimum;
	point /= (this->boundingBox.maximum - this->boundingBox.minimum);
	point *= (float)this->cellsPerSide;

	// Use a predefined array of offsets to calculate all possible neighbors in the grid
	// Usually only one cell will be included but all neighbors must be checked

	std::array<Vector3i, 8> neighbors;
	for (int i = 0; i < 8; i++) {
		Vector3i indices = point + offsets[i];
		for (int j = 0; j < 3; j++) {
			neighbors[i].data[j] = std::clamp(indices.data[j], 0, this->cellsPerSide - 1);
		}
	}
	
	// Only unique neighbor cells are considered

	int uniqueCount = (int)(std::unique(neighbors.begin(), neighbors.end()) - neighbors.begin());
	
	for (int i = 0; i < uniqueCount; i++) {
	
		Vector3i cell = neighbors[i];
		int index = cell.z * (this->cellsPerSide * this->cellsPerSide) + cell.y * this->cellsPerSide + cell.x;
	
		for (int n = 0; n < this->vertexGrid[index].points.size(); n++) {
			if (this->vertexGrid[index].points[n].ID != gridPoint.ID) {
				neighboringPoints.push_back(this->vertexGrid[index].points[n]);
			}
		}
	}
}