#pragma once

#include "Includes.h"

#include "Vector.h"
#include "BoundingVolume.h"



// Special point used only by a vertex deduplication grid
struct VDGPoint {
	Vector3f position;
	int ID;
};

// A single cell of a vertex deduplication grid
struct VDGCell {

	std::vector<VDGPoint> points;
};

class VertexDeduplicationGrid {

private:

	BoundingBox boundingBox;
	int cellsPerSide;

	std::vector<VDGCell> vertexGrid;

public:

	VertexDeduplicationGrid();
	VertexDeduplicationGrid(BoundingBox boundingBox, int cellsPerSide);

	/// <summary>
	/// Insert a point into the grid
	/// </summary>
	/// <param name="gridPoint :">Point to add</param>
	void insertGridPoint(VDGPoint gridPoint);

	/// <summary>
	/// Return all vertices near the one of interest
	/// </summary>
	/// <param name="gridPoint :">Point around which to search</param>
	/// <param name="neighboringPoints :">An array where the neighbors will be stored, must be empty before use</param>
	void getNeighboringGridPoints(VDGPoint gridPoint, std::vector<VDGPoint>& neighboringPoints);
};