#version 460 core

layout (location = 0) out vec4 finalColor;

in flat vec3 color;

void main() {

	finalColor = vec4(color, 1);
}


