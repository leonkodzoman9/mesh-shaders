#version 460 core

layout (location = 0) in vec4 vertexPosition;



float randColor(vec2 co){
    return max(fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453), 0.3);
}



uniform mat4 viewProjection;
uniform mat4 model;

out flat vec3 color;


void main() {

    vec4 position = vec4(vertexPosition.xyz, 1.0);

    gl_Position = viewProjection * model * position;

    color = vec3(randColor(vec2(position.xy)), randColor(vec2(position.yz)), randColor(vec2(position.zw)));
    color /= max(max(color.x, color.y), color.z);
}  