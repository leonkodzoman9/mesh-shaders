#version 460 core

layout (location = 0) out vec4 finalColor;

layout (location = 1) in vertexColors {
	vec4 color;
};

void main() {

    finalColor = color;
}




